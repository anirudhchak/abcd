from django.db import models
from organization_dashboard.models import Organization


class Scenario(models.Model):
    name = models.CharField(max_length=100)
    org_link = models.ForeignKey(Organization, on_delete=models.DO_NOTHING)
